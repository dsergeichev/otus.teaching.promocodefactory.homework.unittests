﻿using AutoFixture;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using FluentAssertions;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Fixture _fixture;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _fixture = new Fixture();
            _fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList().ForEach(b => _fixture.Behaviors.Remove(b));
            _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
        }

        private IServiceProvider BuildServiceCollection()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddDbContext<DataContext>(cfg => cfg.UseInMemoryDatabase("InMemoryDb"));
            serviceCollection.AddTransient(typeof(IRepository<>), typeof(EfRepository<>));
            return serviceCollection.BuildServiceProvider();
        }

        /// <summary>
        /// 1. Если партнер не найден, то также нужно выдать ошибку 404.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerNotFound_ReturnsNotFound()
        {
            //Arrange
            var sp = BuildServiceCollection();
            var partnerId = Guid.NewGuid();

            var request = _fixture.Build<SetPartnerPromoCodeLimitRequest>().Create();

            var controller = new PartnersController(sp.GetRequiredService<IRepository<Partner>>());

            //Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);

            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// 2. Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerNotActive_ReturnsBadRequest()
        {
            //Arrange
            var sp = BuildServiceCollection();
            var partner = _fixture.Build<Partner>()
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>())
                .With(p => p.IsActive, false)
                .Create();

            var repository = sp.GetRequiredService<IRepository<Partner>>();
            await repository.AddAsync(partner);

            var controller = new PartnersController(sp.GetRequiredService<IRepository<Partner>>());

            //Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partner.Id, null);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// 3.1. Если партнеру выставляется лимит и лимит не закончился, то мы должны обнулить количество промокодов, 
        /// которые партнер выдал NumberIssuedPromoCodes.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerLimitSetAndLimitNotExpired_ResetNumberIssuedPromoCodes()
        {
            //Arrange
            var sp = BuildServiceCollection();
            var partnersLimit = _fixture.Build<PartnerPromoCodeLimit>()
                .With(p => p.CancelDate, (DateTime?)null)
                .With(p => p.EndDate, DateTime.Now.AddDays(30))
                .Create();
            var partner = _fixture.Build<Partner>()
                .With(p => p.IsActive, true)
                .With(p => p.NumberIssuedPromoCodes, 123)
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit> { partnersLimit })
                .Create();
            var request = _fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(s => s.Limit, 100)
                .Create();

            var repository = sp.GetRequiredService<IRepository<Partner>>();
            await repository.AddAsync(partner);

            var controller = new PartnersController(sp.GetRequiredService<IRepository<Partner>>());

            //Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            var resultPartnerInDb = await repository.GetByIdAsync(partner.Id);

            //Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            resultPartnerInDb.NumberIssuedPromoCodes.Should().Be(0);
        }

        
        /// <summary>
        /// 3.2. Если партнеру выставляется лимит и лимит закончился, то мы НЕ должны обнулять количество промокодов, 
        /// которые партнер выдал NumberIssuedPromoCodes.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerLimitSetAndLimitExpired_NotResetNumberIssuedPromoCodes()
        {
            //Arrange
            var sp = BuildServiceCollection();
            var partnersLimit = _fixture.Build<PartnerPromoCodeLimit>()
                .With(p => p.CancelDate, (DateTime?)null)
                .With(p => p.EndDate, DateTime.Now.AddDays(-10))
                .Create();
            var partner = _fixture.Build<Partner>()
                .With(p => p.IsActive, true)
                .With(p => p.NumberIssuedPromoCodes, 123)
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit> { partnersLimit })
                .Create();
            var request = _fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(s => s.Limit, 100)
                .Create();

            var repository = sp.GetRequiredService<IRepository<Partner>>();
            await repository.AddAsync(partner);

            var controller = new PartnersController(sp.GetRequiredService<IRepository<Partner>>());

            //Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partner.Id, request);
            var resultPartnerInDb = await repository.GetByIdAsync(partner.Id);

            //Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            resultPartnerInDb.NumberIssuedPromoCodes.Should().Be(123);
        }

        /// <summary>
        /// 4. При установке лимита нужно отключить предыдущий лимит.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerLimitSet_DisablePreviousLimit()
        {
            //Arrange
            var sp = BuildServiceCollection();
            var partnerLimit = _fixture.Build<PartnerPromoCodeLimit>()
                .With(p => p.CancelDate, (DateTime?)null)
                .Create();
            var oldPartnerLimitId = partnerLimit.Id;
            var partner = _fixture.Build<Partner>()
                .With(p => p.IsActive, true)
                .With(p => p.NumberIssuedPromoCodes, 123)
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit> { partnerLimit })
                .Create();
            var request = _fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(s => s.Limit, 100)
                .Create();

            var repository = sp.GetRequiredService<IRepository<Partner>>();
            await repository.AddAsync(partner);

            var controller = new PartnersController(sp.GetRequiredService<IRepository<Partner>>());

            //Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            var resultPartnerInDb = await repository.GetByIdAsync(partner.Id);
            var resultCanceledLimit = resultPartnerInDb.PartnerLimits.FirstOrDefault(x => x.Id == oldPartnerLimitId);

            //Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();

            resultCanceledLimit.CancelDate.Should().NotBeNull();
        }

        /// <summary>
        /// 5. Лимит должен быть больше 0;
        /// </summary>
        /// <returns></returns>
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(int.MinValue)]
        public async Task SetPartnerPromoCodeLimitAsync_RequestLimitLessOrEqualsZero_ReturnsBadRequest(int limit)
        {
            //Arrange
            var sp = BuildServiceCollection();
            var partner = _fixture.Build<Partner>()
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>())
                .With(p => p.IsActive, true)
                .Create();

            var request = _fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(s => s.Limit, limit)
                .Create();

            var repository = sp.GetRequiredService<IRepository<Partner>>();
            await repository.AddAsync(partner);

            var controller = new PartnersController(sp.GetRequiredService<IRepository<Partner>>());

            //Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// 6. Новый лимит должен быть сохранён в базу данных.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_LimitMustBeSaved_CheckInDb()
        {
            //Arrange
            var sp = BuildServiceCollection();
            var partner = _fixture.Build<Partner>()
                .With(p => p.PartnerLimits, new List<PartnerPromoCodeLimit>())
                .With(p => p.IsActive, true)
                .Create();

            var request = _fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(s => s.Limit, 1000)
                .Create();

            var repository = sp.GetRequiredService<IRepository<Partner>>();
            await repository.AddAsync(partner);

            var controller = new PartnersController(sp.GetRequiredService<IRepository<Partner>>());

            //Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            var partnerInDb = await repository.GetByIdAsync(partner.Id);

            //Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();

            partnerInDb.PartnerLimits.Should().ContainSingle(x => x.PartnerId == partner.Id)
                .And.ContainSingle(x => x.Limit == request.Limit)
                .And.ContainSingle(x => x.EndDate == request.EndDate);
        }
    }
}